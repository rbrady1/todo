# Deployment configuration with Kustomize

The project uses [Kustomize](https://kustomize.io/) to generate the Kubernetes manifests.

This tree includes a `base` directory that applies across all environments and a directory for each environment which contains overlays to configure variants.

Each environment consists of a `settings.env` file which can be used to set environment variables.
