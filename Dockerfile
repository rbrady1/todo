FROM python:3.11.2-slim-bullseye as builder

WORKDIR /tmp

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip install poetry==1.4.2
COPY ./pyproject.toml ./pyproject.toml
RUN poetry export --output requirements.txt --without-hashes

FROM python:3.11.2-slim-bullseye

WORKDIR /app

COPY --from=builder /tmp/requirements.txt /app/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

COPY ./src /app/src

CMD ["uvicorn", "src.main:app", "--host", "0.0.0.0", "--port", "8000"]
