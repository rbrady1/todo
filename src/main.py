from fastapi import FastAPI, Response

from .config import settings

app = FastAPI(title=settings.PROJECT_NAME)


@app.api_route("/health", methods=["HEAD", "GET", "OPTIONS"], include_in_schema=False)
def health_check() -> Response:
    return Response(status_code=200)
